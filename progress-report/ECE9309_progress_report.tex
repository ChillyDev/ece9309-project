\documentclass[conference]{IEEEtran}

\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{subcaption}

\graphicspath{{figures/}}
\hypersetup{hidelinks}

\begin{document}
\bibliographystyle{IEEEtran}

\title{Predicting the Popularity of Music from its Characteristics\\
  {\footnotesize Course project for ECE9309 Machine Learning: From Theory to
    Applications}
}

\author{\IEEEauthorblockN{Sarah Al-qaysi}
  \IEEEauthorblockA{\textit{Electrical and Computer Engineering} \\
    \textit{(University of Western Ontario)} \\
London, Canada \\
salqaysi@uwo.ca id:251054425}
\and

\IEEEauthorblockN{Patrick Egan}
  \IEEEauthorblockA{\textit{Electrical and Computer Engineering} \\
    \textit{(University of Western Ontario)} \\
London, Canada \\
pegan3@uwo.ca id:250797509}
\and

\IEEEauthorblockN{Maryam Mohseni}
  \IEEEauthorblockA{\textit{Electrical and Computer Engineering} \\
    \textit{(University of Western Ontario)} \\
London, Canada \\
mmohsen8@uwo.ca id:251186565}
}

\maketitle

\begin{abstract}
  Music like many forms of art has a very diverse spectrum of works with their
  own characteristics.  The Spotify music streaming service provides access to
  a plethora of music with different genres, artists, and eras in which they
  were released.  What’s exciting is that these different musical qualities
  are available both to listen to and retrieve as raw data thanks to the
  Spotify api.  The goal of this project was to explore the relationship
  between these characteristics stored in Spotify to develop models which
  could use this information to determine the popularity of their associated
  songs.  This was initially approached by training models with the dataset.
  Initial results without applying additional pre-processing returned…
  (determine what sort of results should be mentioned here).  To try improving
  results the feature set was augmented by including genres then tailoring the
  dataset via removing songs and artists which did not have genre values.
\end{abstract}

\begin{IEEEkeywords}
Put some index terms here:
Feature Dimensionality Reduction, PCA, Regression, Artificial Neural Networks
\end{IEEEkeywords}

\section{Introduction}
\subsection{Problem Definition}
Music has many different characteristics which has caused it to become a very
diverse medium, but can knowing these characteristics tell us how popular a
particular song will be?   The goal of this project is to investigate which
methods are the best to determine the popularity of a song from its metadata.
\subsection{Motivation}
Popular music can be streamed by users upwards of several billion times while
unpopular music may only be streamed several thousands of times.  Such large
demand suggests that it would be ideal to know in advance which newly released
songs will require more resources allocated.  A system which can predict
higher loads on the service using popularity as a metric for a relative number
of users could aid in the design of load balancing systems by potentially
preparing them for heavy loads.

\section{Related Work}
A work from recently set out to build a model to achieve a goal very similar to
this project.  Matsumoto ``et al'' ~\cite{context_aware_artist_popularity}
created a network model to evaluate the popularity of artists using information
gathered from social media.  This project also used a dataset constructed from
the Spotify api, applying canonical correlation analysis (CCA) to accurately
model relationships between artists ~\cite{context_aware_artist_popularity}.
Additionally it is mentioned that the popularity feature for artists is
calculated on the basis of the artist's audio tracks
~\cite{context_aware_artist_popularity}.  This suggests that artist popularity
could be mapped to the artist feature for each song to provide an additional
helpful feature.


\section{Dataset}
The dataset chosen for this project~\cite{spotify_dataset}
features a list of over 175000 songs pulled from the Spotify api.  This
includes up to 4000 songs from each year between 1921 and 2020.  Although the
songs are not distributed evenly through the years, there are far more entries
in recent years than 1921-1940.  Each item in this dataset includes metadata
about the song as both “quantified numerical values” such as acousticness,
instrumentalness, popularity etc and “categorical values” like artist(s), genre,
etc.  This dataset is also broken down into several datasets which each focus
on a particular feature.
\\ \indent In addition to the base dataset which lists out
songs, there are also datasets for the individual artists, genres and even
years.  These specific datasets present many of the same features as the base
song dataset, but each value is a mean of all values for songs related to the
artist, genre or year.  A very useful feature of these specific datasets is
the \emph{count} feature which represents the number of songs which exist in
the main dataset for the artist, genre, year.  Aside from the main dataset and
three specific additional ones mentioned there is one last dataset which shows
the relationship between artist and genre.  This dataset was very helpful in
improving the base dataset as it provided a means to map artists to genres
so that genres could be used as features.
\\ \indent It should be noted that because this data set was created by
pulling values from the Spotify api.  So new datasets could be created in the
same way.

\section{Methodology}
\subsection{Exploratory Analysis}
The main dataset containing the individual songs and the popularity feature were
the primary focus during the exploratory analysis.  The feature \emph{id} was
immediately dropped for not providing any meaningful information.  \emph{Name}
was considered for potentially being encoded into a dictionary, but because
there were non-english song names this idea was dropped.  Lastly \emph{release date}
was not consistently complete across every song and seemed somewhat redundant
due to the presence of a release year feature.  Initially the \emph{artists} feature
was dropped as well due to the high count of artists generating a very large
number of encoded features, but was reconsidered after the first few models were
not performing well.  Later on the \emph{artists} feature would be used with the
dataset containing the genres for each artist to map \emph{artists} to \emph{genres}
as discussed in \emph{Preprocessing}.
\\ \indent The remaining features which were not dropped were analyzed by their
relationship to \emph{popularity}.  This was achieved by using a heatmap to
visualize the correlations between the numerical music metadata features and
popularity. it can be observed from ``Fig. ~\ref{songHeatmap}'' that the
features with the highest correlation include \emph{year}, \emph{energy},
and \emph{danceability}.
\\ \indent During the mapping of \emph{artists} to \emph{genres} some analysis
of the genre dataset helped ensure that the new genre features which would be
included in the main dataset would be mostly unique.  This was visualized by
using \emph{T-SNE} (t-distributed stochastic neighbor embedding) plots to show
how the genres would cluster together.  \emph{T-SNE} plots are essentially a
method to visualize the clustering of higher dimensional data in a lower
dimensional~\cite{tsne_paper}. Each
identifiable cluster represented a group of genres which were different from
the rest, representing potential new features which could help distinguish
between songs.  Initially these \emph{T-SNE} plots showed that it would be hard
to distinguish between all of the genres because there were few or no
identifiable groups but rather curved lines generated from the lack of
uniqueness between these genres as shown in ``Fig. ~\ref{tsne_genres_before}''.
This lead to a reduction of the genres to only those which appeared the most
frequently.  The result was a substantially smaller dataset of genres with more
uniqueness as shown in ``Fig. ~\ref{tsne_genres_after}''.

\begin{figure}[bp]
  \centerline{\includegraphics[scale=0.20]{music_heatmap.png}}
  \caption{Song Features Heatmap}
  \label{songHeatmap}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[scale=0.05]{genres_t-sne_before.png}
  \caption{Genres Before Reduction T-SNE Plot}
  \label{tsne_genres_before}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[scale=0.05]{genres_t-sne_after.png}
  \caption{Genres After Reduction T-SNE Plot}
  \label{tsne_genres_after}
\end{figure}


\subsection{Preprocessing}
The majority of the pre-processing involved reducing the number of genres to
only those which appeared most frequently and then mapping the artists to these
genres.  Reducing the number of genres involved first purging all artists which
did not have an associated genre.  This was followed by removing songs from the
main dataset which did not have an artist associated with a genre to prune some
incomplete data.
\\ \indent Once all data associated with undefined genres was removed the
genres themselves got reduced.  This was essentially done by selecting a
threshold for the number of appearances the genre had to make using its \emph{count}
feature.  With the genres reduced, the artists and songs were once again pruned
so that only data associated with the selected genres would remain.  The dataset
describing artists with their associated was used to create the mapping from
artists to genres.  Because each artist could be associated with a list of
genres and each song with a list of artists the union join algorithm was used
to apply the mapping.  The \emph{MultiLabelBinarizer} from the scikit-learn~\cite{scikit-learn}
library was used to encode the selected genres into features.
\\ \indent Principal Component Analysis (PCA) provided through the
scikit-learn~\cite{scikit-learn} library was investigated as a means to
reduce the feature dimensionality of the dataset after augmenting it with the
including of genres for each song.  A few different desired sizes to reduce the
number of features to were benchmarked using a basic \emph{LogisticRegression}
model from the scikit-learn~\cite{scikit-learn} library.  It was observed that
applying principal component analysis did not provide a substantial improvement
to classification as shown in ``Table ~\ref{tablePca}''.
\\ \indent To improve classification accuracy the number of classification
models the popularity feature was reduced by redefining the feature as a group
of intervals.  With the original popularity values being integers in the range
of 1 to 100 this transformation was applied by simply applying floor rounding to
each popularity value.

\begin{table}[bp]
  \caption{Accuracy of PCA Reduced Features}
  \label{tablePca}
  \begin{center}
    \begin{tabular}{c|c}
      PCA Output Size & Logistic Regression Test Accuracy \\
      \hline
      No PCA & 40.26\% \\
      60 & 39.61\% \\
      65 & 39.6\% \\
      70 & 40.5\% \\
      75 & 40.42\% \\
      80 & 40.22\% \\
    \end{tabular}
  \end{center}
\end{table}

\subsection{Models}
Classification models were the primary focus due to the popularity of songs
being a discrete range of integers rather than a continuous range of decimals.
The models investigated include the tensorflow~\cite{tensorflow2015-whitepaper}
keras library's Artificial Neural Network, Logistic Regression, XGBoost~\cite{xgboost}
library's XGBClassifier and scikit-learn~\cite{scikit-learn} library's
RandomForestClassifier.  Logistic regression was chosen due to its usefulness
in classification problems.  Artificial Neural Networks allow for the modeling
of patterns in the relationships between features which are evidently present in
heatmap shown in ``Fig. ~\ref{songHeatmap}''.

\subsection{Hyperparameter Tuning}
While tuning model hyperparameters the primary requirements of interest included
using a large number of features and having multiple output classes.  For these
reasons the adam optimizer function and Sparse Categorical Crossentropy
loss function were chosen to be applied to the Artificial Neural Network model.
\\ \indent Tuning the hyperparameters for XGBoost~\cite{xgboost} involved splitting the
dataset and applying Grid Search with Cross Validation to determine ideal values
for the hyperparameters \emph{n\_estimators, max\_depth} and \emph{learning\_rate}.

\section{Results and Analysis}
\subsection{Result Evaluation}
The main methods used to evaluate the performance of the models were the
accuracy score, confusion matrix and F1 score.  Accuracy is a generally
well rounded score for different types of models and provided a simple metric
which was quick to refer to for testing several different hyperparameters and
remapped dataset features on a single model.  In the context of classification
problems the confusion matrix is a powerful tool.  With popularity having
been remapped into interval bins, confusion matrices offered a method to
visualize how misclassifications would occur for different interval bin sizes.

\subsection{Expected Results}
Due to the lower number of features with high correlation to popularity the
initial expected results were a high number of misclassifications.  This created
the expectation that augmenting the dataset would improve classification results.
The addition of genres for each song substantially improved classification
accuracy from roughly 13\% to 40\% in a simple Logistic Regression model.
\\ \indent The scikit-learn~\cite{scikit-learn} Random Tree Classifier model
was expected to evaluate the variables shown to have the strongest correlation
in the heatmap shown in ``Fig. ~\ref{songHeatmap}'' as having the most
importance using the Gini index.  However, it was observed that the \emph{year}
was given substantially more importance over any other variable and the top four
variables did not match the four features with the strongest correlation to
popularity shown in ``Fig. ~\ref{songHeatmap}''.  These observed variable
importance values are provided in ``Fig. ~\ref{forest_var_importance}''.

\begin{figure}[bp]
  \centerline{\includegraphics[scale=0.20]{tree_feature_significance.png}}
  \caption{Variable Importance Evaluated by scikit-learn~\cite{scikit-learn}
    Random Forest Classifier}
  \label{forest_var_importance}
\end{figure}

\section{Next Steps}
\subsection{Remaining Work}
The bulk of remaining work lies in continuing to tune the hyper parameters for
the selected models.  To aid in the process of hyperparameter tuning the
wandb~\cite{wandb} tool will be used for its detailed graphical logging of
results in different experiments on a particular model.  Aside from focusing on
hyperparameters, the dataset will also be augmented once more by adding the
popularity of each artist to their associated songs.  This is in recognition of
the analysis discussed by Matsumoto ``et al''~\cite{context_aware_artist_popularity}.

\subsection{Timeline}
\begin{itemize}
  \item March 22 - Add wandb~\cite{wandb} to model notebooks
  \item March 23 - Add artist popularity to song dataset to help predict song
        popularity
  \item March 26 - Deadline for hyperparameter tuning
\end{itemize}

\bibliography{progress_report_ref}

\end{document}
