from sklearn.decomposition import PCA
pca = PCA(n_components = 20)

reduced_train_df = pca.fit_transform(x_train_scaled)
reduced_test_df = pca.transform(x_test_scaled)
